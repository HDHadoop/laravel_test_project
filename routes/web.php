<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/basket', 'BasketController@index')->name('basket');

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/admin/createdvd', 'AdminController@create_dvd_view')->name('create_dvd');

Route::get('/admin/editdvd', 'AdminController@edit_dvd_view')->name('edit_dvd_all');

Route::get('/admin/editdvd/{id}', 'AdminController@individual_edit_view')->name('edit_dvd');

Route::get('/admin/genre', 'AdminController@genre_view')->name('genre_view');

Route::get('/admin/price', 'AdminController@price_view')->name('price_view');

// Route::get('/', function () {
//     return view('home');
// });

Auth::routes();

// Route::get('/basket', 'BasketController@index');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('genrefilter','HomeController@genre_filter');

Route::post('addToCart','HomeController@add_to_cart');

Route::post('removeFromCart','HomeController@remove_from_cart');

Route::post('basketRemoveFromCart','BasketController@remove_from_cart');

Route::post('createDvd','AdminController@create_dvd')->name('createDvd');

Route::post('updateDvd','AdminController@update_dvd')->name('updateDvd');

Route::post('/admin/updateGenre','AdminController@update_genre')->name('updateGenre');

Route::post('/admin/deleteGenre','AdminController@delete_genre')->name('deleteGenre');

Route::post('createGenre','AdminController@insert_genre')->name('createGenre');

Route::post('/admin/updatePrice','AdminController@update_price')->name('updatePrice');

Route::post('/admin/deletePrice','AdminController@delete_price')->name('deletePrice');

Route::post('createPrice','AdminController@insert_price')->name('createPrice');

Route::post('delete_dvd/{id}','AdminController@delete_dvd')->name('delete_dvd');


