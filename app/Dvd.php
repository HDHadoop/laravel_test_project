<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dvd extends Model
{
    //

    protected $hidden = array('pivot');
    protected $fillable = ['title', 'plot', 'director', 'image', 'rating', 'price_id'];

    public function price()
    {
        return $this->hasOne('App\Price');
    }

    // public function genre_belong()
    // {
    //     return $this->hasMany('App\Genre');
    // }

    public function genre()
    {
        return $this->belongsToMany('App\Genre')->select('name');
    }

    public function genre_full()
    {
        return $this->belongsToMany('App\Genre');
    }
}
