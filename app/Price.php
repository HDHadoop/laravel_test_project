<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    //

    public function dvd()
    {
        return $this->belongsTo('App\Dvd');
    }
}
