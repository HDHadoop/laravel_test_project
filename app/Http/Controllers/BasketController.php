<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dvd;
use App\Price;
use App\Genre;
use Session;

class BasketController extends Controller
{
   	public function index() {
   		$data = $this->get_dvd_data();

   		return view('basket', ['data' => $data]);
   	}

   	public function get_dvd_data() {
 			if(Session::has('dvd_id')) {
	        $data['session'] = session('dvd_id');
	        $data['dvds'] = Dvd::find($data['session']);
	    } else {
     	   $data['session'] = [];
     	   $data['dvds'] = [];
     	   $data['totalPrice'] = [];
		  }
		  $data['totalPrice'] = null;
   		foreach($data['dvds'] as $dvd) {
	        $dvd['price'] = Price::where('id', '=', $dvd['price_id'])->value('price');
	        $dvd['genre'] = $dvd->genre()->get();
	        $data['totalPrice'] = $data['totalPrice'] + $dvd['price'];
      	}

      	return $data;   
   	}

   	public function remove_from_cart(request $response) {
      Session::forget('dvd_id.' . $response->id);

      $data = $this->get_dvd_data();

 			$html = view('partial.basket', compact('data'))->render();

      return response()->json(compact('html'));
    }
}
