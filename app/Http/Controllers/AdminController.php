<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Dvd;
use App\Price;
use App\Genre;
use App\Dvd_genre;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;


class AdminController extends Controller
{
		/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
    	return view('admin');
    }

    public function create_dvd_view() {
    	$data['price'] = Price::all();
    	$data['genre'] = Genre::all();
    	$data['message'] = '';
    	return view('admin.create_dvd', ['data' => $data]);
    }

    public function edit_dvd_view() {
  		$data['dvds'] = Dvd::simplePaginate(15);

      //Using list of Dvds, gets the appropriate Genre and Price.
      foreach($data['dvds'] as $dvd) {
        $dvd['price'] = Price::where('id', '=', $dvd['price_id'])->value('price');
        $dvd['genre'] = $dvd->genre()->get();
      } 

    	return view('admin.edit_dvd', ['data' => $data]);
    }

    public function individual_edit_view($id) {
    	$data['dvd'] = Dvd::find($id);
    	$data['price'] = Price::all();
    	$data['genre'] = Genre::all();
    	$data['message'] = '';

      $data['dvd']['genre'] = $data['dvd']->genre()->get()->toArray();

    	return view('admin.individual_dvd_edit', ['data' => $data]);
    }

    public function genre_view() {
    	$data['genres'] = Genre::simplePaginate(15);

    	return view('admin.genre', ['data' => $data]);
    }

    public function price_view() {
      $data['prices'] = Price::simplePaginate(15);

      return view('admin.price', ['data' => $data]);
    }

    public function update_dvd(Request $request) {
    	$dvd_id = $request->input('dvdId');
    	$data['dvd']['title'] = $request->input('dvdTitle');
  		$data['dvd']['plot'] = $request->input('dvdPlot');
  		$data['dvd']['directors'] = $request->input('dvdDirectors');
  		$data['dvd']['rating'] = $request->input('dvdRating');
  		$data['dvd']['price_id'] = $request->input('dvdPrice');
  		$data['dvd_genre']['genre'] = $request->input('dvdGenre');

  		// TODO - pass the data to check if any errors

  		$errors = $this->check_data($data);
  		$data['message'] = '';

  		if(!$errors) {
  			if($request->file('dvdImage') !== NULL) {
					$data['dvd']['image'] = $this->upload_dvd_image($request->file('dvdImage')->getClientOriginalName(), $request->file('dvdImage')->getRealPath(), $data['dvd']['title']);
				}
				$this->update_dvd_data($dvd_id, $data['dvd']);
  			$this->update_dvd_genre($dvd_id, $data['dvd_genre']['genre']);
  		} else {
  			$data['message'] = 'Please ensure all fields are entered/selected.';
  		}

    	return redirect()->action('AdminController@individual_edit_view', ['id' => $dvd_id]);
    }

    public function create_dvd(Request $request) {
    	//Set the price and genre
    	$data['price'] = Price::all();
  		$data['genre'] = Genre::all();

  		//Set the post data into variables inside the data array.
  		$data['dvd']['title'] = $request->input('dvdTitle');
  		$data['dvd']['plot'] = $request->input('dvdPlot');
  		$data['dvd']['directors'] = $request->input('dvdDirectors');
  		$data['dvd']['rating'] = $request->input('dvdRating');
  		$data['dvd']['price'] = $request->input('dvdPrice');

  		$data['dvd_genre']['genre'] = $request->input('dvdGenre');

  		$errors = $this->check_data($data);
  		$data['message'] = '';

  		if(!$errors) {
				$data['dvd']['image'] = $this->upload_dvd_image($request->file('dvdImage')->getClientOriginalName(), $request->file('dvdImage')->getRealPath(), $data['dvd']['title']);
				$dvd_id = $this->insert_dvd_data($data['dvd']);
  			$this->insert_dvd_genre($dvd_id, $data['dvd_genre']['genre']);
        return redirect()->route('edit_dvd', ['id' => $dvd_id]);
  		} else {
  			$data['message'] = 'Please ensure all fields are entered/selected.';
        return redirect()->route('create_dvd');
  		}

    	// return view('admin', ['data' => $data]);
    }

    public function upload_dvd_image($image_name, $image_file_path, $dvd_title) {
      //TODO - rather than use 2 paramereters, one for name and one for path, just pass the image object itself through.
    	$img = Image::make($image_file_path);
      $img_extension = $ext = pathinfo($image_name, PATHINFO_EXTENSION);
				// save image
    	$image = $dvd_title . '.' . $img_extension;
			if(\File::exists('../public/images/' . '.' . $image)) {
				$image = $dvd_title . Carbon::now()->timestamp . '.' . $img_extension;
				$img->save('../public/images/' . $image);
			} else {
				$img->save('../public/images/' . $image);
			}

			return $image;
    }

    public function insert_dvd_data($dvd) {
    	$new_dvd = new Dvd;
    	
    	$new_dvd->title = $dvd['title'];
    	$new_dvd->plot = $dvd['plot'];
    	$new_dvd->director = $dvd['directors'];
    	$new_dvd->rating = $dvd['rating'];
    	$new_dvd->image = $dvd['image'];
    	$new_dvd->price_id = $dvd['price'];

    	$new_dvd->save();

    	return $new_dvd->id;
    }

    public function insert_dvd_genre($dvd_id, $genre_ids) {
    	foreach($genre_ids as $id) {
    		$this->insert_dvd_genre_single($dvd_id, $id);
    	}

    }

    public function insert_dvd_genre_single($dvd_id, $genre_id) {
    	$new_dvd_genre = new Dvd_genre;

  		$new_dvd_genre->dvd_id = $dvd_id;
  		$new_dvd_genre->genre_id = $genre_id;

  		$new_dvd_genre->save();
    }

    public function insert_genre(Request $request) {
      $genre_name = $request->input('name');
      $count = Genre::where('name', '=', $genre_name)->count();
      if($count === 0 && strlen($genre_name) > 0) {
        $genre = new Genre;
        $genre->name = $genre_name;
        $genre->save();
      }

      return redirect()->action('AdminController@genre_view');
    }

    public function update_genre(request $response) {
      $genre = Genre::find($response->id);

      $genre->name = $response->name;

      $genre->save();
    }

    public function delete_genre(request $response) {
      $this->delete_associated_genre_by_id($response->id);
      Genre::find($response->id)->delete();
    }

    public function delete_associated_genre_by_id($genre_id) {
      $associated_genres = Dvd_genre::where('genre_id', '=', $genre_id)->get();
      foreach($associated_genres as $genre) {
        $genre->delete();
      }
    }

    public function insert_price(Request $request) {
      $price_name = $request->input('price');
      $count = Price::where('price', '=', $price_name)->count();
      if($count === 0 && strlen($price_name) > 0) {
        $price = new Price;
        $price->price = $price_name;
        $price->save();
      }

      return redirect()->action('AdminController@price_view');
    }

    public function update_price(request $response) {
      $price = Price::find($response->id);

      $price->price = $response->price;

      $price->save();
    }

    public function delete_price(request $response) {
      if($this->associated_price_by_id($response->id)) {
        $data['status'] = 'success';
        Price::find($response->id)->delete();
      } else {
        $data['status'] = 'error';
      }

      header('Content-type: application/json');
      echo json_encode($data);
    }

    public function associated_price_by_id($price_id) {
      $associated_prices = Dvd::where('price_id', '=', $price_id)->count();
      if($associated_prices === 0) {
        return true;
      } else {
        return false;
      }
    }

    public function update_dvd_data($dvd_id, $dvd_data) {
    	Dvd::find($dvd_id)->update($dvd_data);
    }

    public function update_dvd_genre($dvd_id, $genre_ids) {
    	$associated_genres = Dvd_genre::where('dvd_id','=',$dvd_id)->get();
    	foreach($associated_genres as $genre) {
    		if(!in_array($genre->genre_id, $genre_ids)) {
    			$genre->delete();
    		}
    	}

    	foreach($genre_ids as $id) {
    		$count = Dvd_genre::where('dvd_id','=',$dvd_id)->where('genre_id', '=', $id)->count();
    		if($count === 0) {
  				$this->insert_dvd_genre_single($dvd_id, $id);
    		}
    	}
    }

    public function check_data($data) {
    	$errors = false;

    	foreach($data['dvd'] as $dvd) {
				if(!strlen($dvd) > 0) {
					$errors = true;
					break;
				}
  		}
  		
  		if(count($data['dvd_genre']['genre']) === 0) {
  			$errors = true;
  		}

  		return $errors;
    }

    public function delete_dvd($id) {
      $genres = Dvd_genre::where('dvd_id', '=', $id)->get();
      foreach($genres as $genre) {
        $genre->delete();
      }
      Dvd::find($id)->delete();
    }
}
