<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dvd;
use App\Price;
use App\Genre;
use Session;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = $this->get_dvd_data();

      return view('home', ['data' => $data]);
    }

    public function get_dvd_data() {
      $data['dvds'] = Dvd::simplePaginate(15);
      $data['genre'] = Genre::select('name')->get();

      // Session::forget('dvd_id');
      if(Session::has('dvd_id')) {
        $data['session'] = session('dvd_id');
      } else {
        $data['session'] = [];
      }

      //Using list of Dvds, gets the appropriate Genre and Price.
      foreach($data['dvds'] as $dvd) {
        $dvd['price'] = Price::where('id', '=', $dvd['price_id'])->value('price');
        $dvd['genre'] = $dvd->genre()->get();
      } 

        return $data;   
    }

    public function get_genre_id_by_name($genre_name) {
      return Genre::where('name', '=', $genre_name)->value('id');
    }

    public function genre_filter(request $response) {
      $genre_name = $response->name;
      $genre_id = $this->get_genre_id_by_name($genre_name);
      $data['dvds'] = Dvd::whereHas('genre', function ($query) use ($genre_id) {
        $query->where('genre_id', '=', $genre_id);
      })->get();

      foreach($data['dvds'] as $dvd) {
        $dvd['price'] = Price::where('id', '=', $dvd['price_id'])->value('price');
      }

      if(Session::has('dvd_id')) {
        $data['session'] = session('dvd_id');
      } else {
        $data['session'] = [];
      }

      $html = view('partial.dvd', compact('data'))->render();

      return response()->json(compact('html'));
    }

    public function add_to_cart(request $response) {
      $dvd_id = session('dvd_id');
      Session::put('dvd_id', array_add($dvd_id = Session::get('dvd_id'), $response->id, $response->id));

      return session('dvd_id');
    }

    public function remove_from_cart(request $response) {
      Session::forget('dvd_id.' . $response->id);

      return session('dvd_id');
    }
}
