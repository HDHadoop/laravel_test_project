<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dvd_genre extends Model
{
    //
    protected $table = "dvd_genre";

    public function dvd()
    {
        return $this->belongsToMany('App\Dvd');
    }

    public function genre()
    {
        return $this->belongsToMany('App\Genre');
    }
}
