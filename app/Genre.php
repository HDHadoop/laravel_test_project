<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    //

    protected $hidden = array('pivot');

    public function dvd()
    {
        return $this->belongsTo('App\Dvd');
    }

    public function dvds()
    {
        return $this->hasMany('App\Dvd');
    }
}
