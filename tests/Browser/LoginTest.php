<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('HadoopBusters');
        });
    }

    // Tests that you can successfully login with correct credentials
    public function testLogin() {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->clickLink('Login')
                    ->type('email','hardeepsingh7893@gmail.com')
                    ->type('password','testpass')
                    ->press('Login')
                    ->assertSee('Hardeep');
        });
    }

    public function testLogout() {
        $this->browse(function (Browser $browser) {
            $browser->clickLink('Logout')
                    ->assertSee('Login');
        });
    }
}
