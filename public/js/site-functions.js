$(document).ready(function() {
  $('select').niceSelect();

  $.fn.hasAttr = function(name) {  
   return this.attr(name) !== undefined;
	};

  $('.genre-filter').change(function() {
  	var item=$(this);
  	var $container = $('.dvds-holder');

  	$container.fadeOut();

  	$.ajax({
	    method: 'POST', // Type of response and matches what we said in the route
	    url: 'genrefilter', // This is the url we gave in the route
	    data: {'name' : item.val(), "_token": $('#token').val()}, // a JSON object to send back
	    success: function(response){ // What to do if we succeed 
	        $('.dvds-holder').html(response.html);
	        $container.fadeIn();
	    },
	    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
	        console.log(JSON.stringify(jqXHR));
	        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
	    }
		});
	});

  $(document).on('click','.add-to-cart',function(){
  	var item = $(this);
  	var dvd_id = $(this).attr('dvd_id');

  	$.ajax({
	    method: 'POST', // Type of response and matches what we said in the route
	    url: 'addToCart', // This is the url we gave in the route
	    data: {'id' : dvd_id, "_token": $('#token').val()}, // a JSON object to send back
	    success: function(response){ // What to do if we succeed
	        var parent = $(item.parent());
	        var dvdName = $(parent).find('h1').text();
	        $(item).remove();
	        $(parent).append('<button dvd_id="' + dvd_id + '" class="btn btn-danger remove-from-cart">Remove from cart</button>');
	        $(parent).find('.basket-change').remove();
	        $(parent).append('<label class="basket-change"> ' + dvdName + ' has been added to your basket </label>');
	        
	        // TODO - Need to update the view with text about this. Also, maybe a remove from cart? reverse operation.
	    },
	    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
	        console.log(JSON.stringify(jqXHR));
	        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
	    }
		});
  });

  $(document).on('click','.remove-from-cart',function(){
  	var item = $(this);
  	var dvd_id = $(this).attr('dvd_id');

  	var url = ($(this).hasAttr('basket_button') ? "basketRemoveFromCart" : "removeFromCart");

  	$.ajax({
	    method: 'POST', // Type of response and matches what we said in the route
	    url: url, // This is the url we gave in the route
	    data: {'id' : dvd_id, "_token": $('#token').val()}, // a JSON object to send back
	    success: function(response){ // What to do if we succeed
	    	console.log(response);
        if($(item).hasAttr('basket_button')) {
        	basket_after_remove_cart(dvd_id, item, response);
        } else {
        	normal_after_remove_cart(dvd_id, item);
        }
	    },
	    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
	        console.log(JSON.stringify(jqXHR));
	        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
	    }
		});
  });

  function normal_after_remove_cart(dvd_id, item) {
  	var parent = $(item.parent());
    var dvdName = $(parent).find('h1').text();
    $(item).remove();
    $(parent).append('<button dvd_id="' + dvd_id + '" class="btn btn-primary add-to-cart">Add to cart</button>');
    $(parent).find('.basket-change').remove();
    $(parent).append('<label class="basket-change"> ' + dvdName + ' has been removed from your basket </label>');
  }

  function basket_after_remove_cart(dvd_id, item, response) {
  	var parent = $(item.parent());
    var dvdName = $(parent).find('h1').text();
    $(item).remove();
    $(parent).append('<button dvd_id="' + dvd_id + '" class="btn btn-primary add-to-cart">Add to cart</button>');
    $(parent).find('.basket-change').remove();
    $(parent).append('<label class="basket-change"> ' + dvdName + ' has been removed from your basket </label>');	

    $('.basket-holder').html(response.html);
  }

  $(document).on('click','.delete-genre',function(){
    var item = $(this);
    var container = item.parent().parent().parent();
    var genre_id = $(this).attr('genre_id');

    $.ajax({
      method: 'POST', // Type of response and matches what we said in the route
      url: 'deleteGenre', // This is the url we gave in the route
      data: {'id' : genre_id, "_token": $('#token').val()}, // a JSON object to send back
      success: function(response){ // What to do if we succeed
        container.fadeOut();
      },
      error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
          console.log(JSON.stringify(jqXHR));
          console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
  });

  $(document).on('click','.update-genre',function(){
    var item = $(this);
    var container = item.parent().parent().parent();
    var genre_id = $(this).attr('genre_id');
    var genre_name = item.parent().parent().parent().find('.genre-name').val();

    $.ajax({
      method: 'POST', // Type of response and matches what we said in the route
      url: 'updateGenre', // This is the url we gave in the route
      data: {'id' : genre_id, 'name' : genre_name, "_token": $('#token').val()}, // a JSON object to send back
      success: function(response){ // What to do if we succeed
        container.append('<p class="updated">' + genre_name + ' Has been updated </p>');
      },
      error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
          console.log(JSON.stringify(jqXHR));
          console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
  });

  $(document).on('click','.delete-price',function(){
    var item = $(this);
    var container = item.parent().parent().parent();
    var price_id = $(this).attr('price_id');

    $.ajax({
      method: 'POST', // Type of response and matches what we said in the route
      url: 'deletePrice', // This is the url we gave in the route
      data: {'id' : price_id, "_token": $('#token').val()}, // a JSON object to send back
      success: function(response){ // What to do if we succeed
        if(response.status == 'success'){
          container.fadeOut();
        }else if(response.status == 'error'){
          container.append('<p class="updated">Price is currently being used!</p>');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
          console.log(JSON.stringify(jqXHR));
          console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
  });

  $(document).on('click','.update-price',function(){
    var item = $(this);
    var container = item.parent().parent().parent();
    var price_id = $(this).attr('price_id');
    var price_name = item.parent().parent().parent().find('.price-name').val();

    $.ajax({
      method: 'POST', // Type of response and matches what we said in the route
      url: 'updatePrice', // This is the url we gave in the route
      data: {'id' : price_id, 'price' : price_name, "_token": $('#token').val()}, // a JSON object to send back
      success: function(response){ // What to do if we succeed
        container.append('<p class="updated">Updated to: ' + price_name + '</p>');
      },
      error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
          console.log(JSON.stringify(jqXHR));
          console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
  });

  $(document).on('click','#test-node',function(){

    $.ajax({
      method: 'GET', // Type of response and matches what we said in the route
      url: 'http://localhost:3000/tasks', // This is the url we gave in the route
      // data: {'id' : price_id, 'price' : price_name, "_token": $('#token').val()}, // a JSON object to send back
      success: function(response){ // What to do if we succeed
        console.log(response);
      },
      error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
          console.log(JSON.stringify(jqXHR));
          console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
  });

});
