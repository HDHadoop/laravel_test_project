@if (count($data['dvds']) !== 0)
  @foreach ( $data['dvds'] as $dvd )
    <div class="col-xs-12 dvd-container">
        <div class="col-xs-4 img-container">
            <img class="img-responsive" src="{{ asset('images/' . $dvd['image']) }}" />
        </div>

        <div class=col-xs-8 text-container">
            <h1>{{ $dvd['title'] }}</h1>
            <p>{{ $dvd['plot'] }}</p>
            <p>Genres: 
              @php
                  $counter = 0;
              @endphp
              @foreach($dvd['genre'] as $genre)
                @php
                  if($counter != 0) {
                    echo ',';
                  }
                  $counter++;
                @endphp
                {{ $genre['name'] }}
              @endforeach
            </p>
            <p>Directors: {{ $dvd['director'] }}</p>
            <p>Price: £{{ $dvd['price'] }}</p>
            @if (!in_array($dvd['id'], $data['session']))
             <button dvd_id="{{ $dvd['id'] }}" class="btn btn-primary add-to-cart">Add to cart</button>
            @else 
             <button dvd_id="{{ $dvd['id'] }}" class="btn btn-danger remove-from-cart">Remove from cart</button>
            @endif
        </div>

    </div>
  @endforeach
@else
  <label>No results found</label>
@endif

