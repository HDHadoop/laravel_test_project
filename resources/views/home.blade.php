@extends('layouts.app')

@section('content')
  <div class="container dvds-page">
  <button id="test-node" class="btn btn-success">Test</button>
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Dvds 
                      <div class="float-right dvd-filters">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <select class='genre-filter'>
                            @foreach( $data['genre'] as $genre)
                                <option> {{ $genre['name'] }} </option>
                            @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="panel-body dvds-holder">
                      @include('partial.dvd')
                      {{ $data['dvds']->links() }}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
