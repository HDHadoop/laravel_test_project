@extends('layouts.app')

@section('content')
  <div class="container basket-page">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Basket 
                      <div class="float-right">
                      	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                      </div>
                  </div>
                  <div class="panel-body basket-holder">
                  	@include('partial.basket')
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
