@extends('layouts.app')

@section('content')
  <div class="container admin-page">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Admin Control Panel
                      <div class="float-right">
                      	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                      </div>
                  </div>
                  <div class="panel-body content-upload-container">
                    <div class="col-xs-12 col-sm-4">
                      <a href="{{ route('create_dvd') }}">
                        <button class="btn btn-primary">
                          Create Dvd
                        </button>
                      </a>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                      <a href="{{ route('edit_dvd_all') }}">
                        <button class="btn btn-primary">
                          Edit Dvds
                        </button>
                      </a>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                      <a href="{{ route('genre_view') }}">
                        <button class="btn btn-primary">
                          Edit Genres
                        </button>
                      </a>
                    </div>

                    <div class="col-xs-12 col-sm-4 margin-top-20">
                      <a href="{{ route('price_view') }}">
                        <button class="btn btn-primary">
                          Edit Prices
                        </button>
                      </a>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
