@extends('layouts.app')

@section('content')
  <div class="container admin-edit-page-all">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Admin Panel - Edit a Dvd
                      <div class="float-right">
                      	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                      </div>
                  </div>
                  <div class="panel-body content-upload-container">
                    @include('admin.dvd')
                    {{ $data['dvds']->links() }}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
