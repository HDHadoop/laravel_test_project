@extends('layouts.app')

@section('content')
  <div class="container admin-edit-page">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Admin Panel - Update Dvd.
                      <div class="float-right">
                      	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                      </div>
                  </div>
                  <div class="panel-body content-upload-container">
                  	<form id="logout-form" action="{{ route('updateDvd') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="dvdId" id="dvdId" value="{{ $data['dvd']['id'] }}">
                        <div class="form-group">
                          <label for="dvdTitle">Dvd Title*</label>
                          <input type="title" class="form-control" name="dvdTitle" id="dvdTitle" placeholder="Enter Title" value="{{ $data['dvd']['title'] }}">
                        </div>
                        <div class="form-group">
                          <label for="dvdPlot">Dvd Plot*</label>
                          <input type="title" class="form-control" name="dvdPlot" id="dvdPlot" placeholder="Enter Plot" value="{{ $data['dvd']['plot'] }}">
                        </div>
                        <div class="form-group">
                          <label for="dvdDirectors">Dvd Director*</label>
                          <input type="title" class="form-control" name="dvdDirectors" id="dvdDirectors" placeholder="Enter Directors" value="{{ $data['dvd']['director'] }}">
                        </div>
                        <div class="form-group">
                          <label for="dvdImage">Dvd Image*</label>
                          <input type="file" class="form-control-file" name="dvdImage" id="dvdImage">
                          <img class="img-responsive" src="{{ asset('images/' . $data['dvd']['image']) }}" />
                        </div>
                        <div class="form-group">
                          <label for="dvdRating">Dvd Rating*</label>
                          <input type="title" class="form-control" name="dvdRating" id="dvdRating" placeholder="Enter Rating" value="{{ $data['dvd']['rating'] }}">
                        </div>
                        <div class="form-group genre">
                          <label>Dvd Genres*</label>
                          <div class="genre-checkbox">
                            @foreach($data['genre'] as $genre)
                              @php $checked = ''; @endphp
                              @foreach($data['dvd']['genre'] as $dvd_genre)
                                @if(in_array($genre['name'], $dvd_genre))
                                  @php $checked = 'checked=checked' @endphp
                                @endif
                              @endforeach
                              <input type="checkbox" name="dvdGenre[]" value="{{ $genre['id'] }}" {{ $checked }}> <span> {{ $genre['name'] }} </span>
                            @endforeach
                          </div>    
                                          
                        </div>
                        <div class="form-group price">
                          <label for="dvdPrice">Dvd Price*</label>
                          <select name="dvdPrice" class='dvd-price-dropdown'>
                            @foreach( $data['price'] as $price)
                              @if($data['dvd']['price_id'] === $price['id'])
                                <option value="{{ $price['id'] }}" selected="selected"> £{{ $price['price'] }} </option>
                              @else
                                <option value="{{ $price['id'] }}"> £{{ $price['price'] }} </option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group form-submit">
                          <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
