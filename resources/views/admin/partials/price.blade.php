<label>Add Price:</label>
<form id="create-price-form" action="{{ route('createPrice') }}" method="POST">
  {{ csrf_field() }}
  <div class="col-xs-12">
    <div class="col-xs-10">
      <input type="title" class="form-control price-name" name="price" placeholder="Enter Price..." />
    </div>
    <div class="col-xs-2">
      <button class="btn btn-success create-price">Create</button>
    </div>
  </div>
</form>
<label>Prices:</label>
@foreach($data['prices'] as $price)
  <div class="col-xs-12 individual-price-container">
    <div class="col-xs-8">
      <input type="title" class="form-control price-name" name="price" value="{{ $price->price }}">
    </div>
    <div class="col-xs-4">
      <div class="col-xs-6">
        <button type="button" class="btn btn-danger delete-price" price_id="{{ $price->id }}">
          Delete
        </button>
      </div>
      <div class="col-xs-6">
        <button type="button" class="btn btn-success update-price" price_id="{{ $price->id }}">
          Update
        </button>
      </div>
    </div>
  </div>
@endforeach