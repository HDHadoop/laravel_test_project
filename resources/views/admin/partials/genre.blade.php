<label>Add Genre:</label>
<form id="create-genre-form" action="{{ route('createGenre') }}" method="POST">
  {{ csrf_field() }}
  <div class="col-xs-12">
    <div class="col-xs-10">
      <input type="title" class="form-control genre-name" name="name" placeholder="Enter Genre..." />
    </div>
    <div class="col-xs-2">
      <button class="btn btn-success create-genre">Create</button>
    </div>
  </div>
</form>
<label>Genres:</label>
@foreach($data['genres'] as $genre)
  <div class="col-xs-12 individual-genre-container">
    <div class="col-xs-8">
      <input type="title" class="form-control genre-name" name="genre" value="{{ $genre->name }}">
    </div>
    <div class="col-xs-4">
      <div class="col-xs-6">
        <button type="button" class="btn btn-danger delete-genre" genre_id="{{ $genre->id }}">
          Delete
        </button>
      </div>
      <div class="col-xs-6">
        <button type="button" class="btn btn-success update-genre" genre_id="{{ $genre->id }}">
          Update
        </button>
      </div>
    </div>
  </div>
@endforeach