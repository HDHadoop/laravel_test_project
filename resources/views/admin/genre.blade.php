@extends('layouts.app')

@section('content')
  <div class="container admin-price-page">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Admin Panel - Genres
                  </div>
                  <div class="panel-body content-upload-container">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    @include('admin.partials.genre')
                    {{ $data['genres']->links() }}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
