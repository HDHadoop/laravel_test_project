@extends('layouts.app')

@section('content')
  <div class="container admin-create-page">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Admin Panel - Create a Dvd.
                      <div class="float-right">
                      	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                      </div>
                  </div>
                  <div class="panel-body content-upload-container">
                  	<form id="logout-form" action="{{ route('createDvd') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                          <label for="dvdTitle">Dvd Title*</label>
                          <input type="title" class="form-control" name="dvdTitle" id="dvdTitle" placeholder="Enter Title">
                        </div>
                        <div class="form-group">
                          <label for="dvdPlot">Dvd Plot*</label>
                          <input type="title" class="form-control" name="dvdPlot" id="dvdPlot" placeholder="Enter Plot">
                        </div>
                        <div class="form-group">
                          <label for="dvdDirectors">Dvd Director*</label>
                          <input type="title" class="form-control" name="dvdDirectors" id="dvdDirectors" placeholder="Enter Directors">
                        </div>
                        <div class="form-group">
                          <label for="dvdImage">Dvd Image*</label>
                          <input type="file" class="form-control-file" name="dvdImage" id="dvdImage">
                        </div>
                        <div class="form-group">
                          <label for="dvdRating">Dvd Rating*</label>
                          <input type="title" class="form-control" name="dvdRating" id="dvdRating" placeholder="Enter Rating">
                        </div>
                        <div class="form-group genre">
                          <label>Dvd Genres*</label>
                          <div class="genre-checkbox">
                            @foreach($data['genre'] as $genre)
                              <input type="checkbox" name="dvdGenre[]" value="{{ $genre['id'] }}"> <span> {{ $genre['name'] }} </span>
                            @endforeach
                          </div>                    
                        </div>
                        <div class="form-group price">
                          <label for="dvdPrice">Dvd Price*</label>
                         <select name="dvdPrice" class='dvd-price-dropdown'>
                            @foreach( $data['price'] as $price)
                                <option value="{{ $price['id'] }}"> £{{ $price['price'] }} </option>
                            @endforeach
                        </select>
                        </div>
                        <div class="form-group form-submit">
                          <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </form>
                    <label class="error-message">{{ $data['message'] }}</label>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
