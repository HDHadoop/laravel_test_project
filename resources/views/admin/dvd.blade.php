@if (count($data['dvds']) !== 0)
  @foreach ( $data['dvds'] as $dvd )
    <a href="{{ route('edit_dvd', $dvd['id']) }}">
      <div class="col-xs-12 dvd-container">
        <div class="col-xs-4 img-container">
            <img class="img-responsive" src="{{ asset('images/' . $dvd['image']) }}" />
        </div>

        <div class=col-xs-8 text-container">
            <h1>{{ $dvd['title'] }}</h1>
            <p>{{ $dvd['plot'] }}</p>
            <p>Genres: 
              @php
                  $counter = 0;
              @endphp
              @foreach($dvd['genre'] as $genre)
                @php
                  if($counter != 0) {
                    echo ',';
                  }
                  $counter++;
                @endphp
                {{ $genre['name'] }}
              @endforeach
            </p>
            <p>Directors: {{ $dvd['director'] }}</p>
            <p>Price: £{{ $dvd['price'] }}</p>
            <form id="logout-form" action="{{ route('delete_dvd', $dvd['id']) }}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <button class="btn btn-danger">Delete</button>
            </form>
        </div>
    </div>
    </a>
  @endforeach
@else
  <label>No results found</label>
@endif

